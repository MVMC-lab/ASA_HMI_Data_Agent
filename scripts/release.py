import conf
from env import ENV
from util import *

import requests
from asa_hmi_data_agent import __version__ as VERSION

def release():
    tag = VERSION
    gitlab_pac = ENV.gitlab.pac

    # 創建release及tag
    r = requests.post(
        url='https://gitlab.com/api/v4/projects/8601172/releases',
        headers={
            'Private-Token': gitlab_pac
        },
        params={
            'name': tag,
            'tag_name': tag,
            'ref': 'master',
            'description': '55'
        }
    )
    print('==== 創建tag結果 =================================================')
    print(r.json())

    # 上傳檔案
    filename = 'dist/ASA_HMI_Data_Agent_v{}.zip'.format(VERSION)

    r = requests.post(
        url='https://gitlab.com/api/v4/projects/8601172/uploads',
        headers={
            'Private-Token': gitlab_pac,
        },
        files={
            'file': open(filename, 'rb')
        }
    )
    print('==== 上傳檔案結果 ================================================')
    print(r.json())
    
    name = r.json()['alt']
    url = r.json()['url']
    download_url = "https://gitlab.com/MVMC-lab/hmi/ASA_HMI_Data_Agent{}".format(url)

    # 創建links
    r = requests.post(
        url='https://gitlab.com/api/v4/projects/8601172/releases/{}/assets/links'.format(
            tag
        ),
        headers={
            'Private-Token': gitlab_pac
        },
        params={
            'name': name,
            'url': download_url
        }
    )
    print('==== 創建links結果 ===============================================')
    print(r.json())


def main():
    release()

if __name__ == '__main__':
    main()

from asa_hmi_data_agent.ui.ui_adt_settings import Ui_MainWidgetAdtSettings
from asa_hmi_data_agent import __version__ as VSRSION
from PyQt5.QtCore import pyqtSlot, QThread, pyqtSignal, QObject
from PyQt5.QtWidgets import QFileDialog
from asa_hmi_data_agent.listport import getAvailableSerialPorts
from time import sleep
import serial
import math
import requests

PID = '8601172'

class DownloadThread(QThread):
    sigStart = pyqtSignal(int)
    sigStepDone = pyqtSignal(int)
    sigAllDone = pyqtSignal()

    def __init__(self):
        super(QThread, self).__init__()

    def start(self, url, filename):
        self.url = url
        self.filename = filename
        super(DownloadThread, self).start()

    def run(self):
        # 下載檔案
        r = requests.get(self.url, stream=True)
        total_size = int(r.headers.get('content-length', 0))
        block_size = 2048
        t = 0
        wrote = 0
        times = math.ceil(total_size//block_size)
        self.sigStart.emit(times)

        with open(self.filename, 'wb') as f:
            for data in r.iter_content(block_size):
                wrote = wrote + len(data)
                f.write(data)
                t = t + 1
                self.sigStepDone.emit(t)

        if total_size != 0 and wrote != total_size:
            print("ERROR, something went wrong")


class AdtSettings(QObject):
    widget = Ui_MainWidgetAdtSettings()
    signalTermNumApply = pyqtSignal(int)

    def __init__(self):
        super(AdtSettings, self).__init__()

    def setupUi(self, widget):
        self.widget.setupUi(widget)

    def init(self):
        self.dth = DownloadThread()
        self.widget.pushButton_termNumApply.clicked.connect(self.termNumApply)
        self.widget.pushButton_download.clicked.connect(self.trig_download)
        self.dth.sigStart[int].connect(self.ui_download_start)
        self.dth.sigStepDone[int].connect(
            self.widget.progressBar_download.setValue)

        self.update_version()

    def update_version(self):
        self.widget.label_currentVersion.setText(VSRSION)

        try:
            url = 'https://gitlab.com/api/v4/projects/{pid}/repository/tags'.format(
                pid=PID
            )
            least_tag = requests.get(url).json()[0]['name']
        except requests.exceptions.ConnectionError:
            self.widget.label_lastestVersion.setText('無法取得最新版本資訊，請檢察網路')
        else:
            self.least_version = least_tag
            self.widget.label_lastestVersion.setText(self.least_version)
            if least_tag == VSRSION:
                self.widget.label_extraInfo.setText('已經是最新版本了！')
                self.widget.pushButton_download.setEnabled(False)
            else:
                self.widget.label_extraInfo.setText('點擊左方按鈕下載{}壓縮檔'.format(self.least_version))
                self.widget.pushButton_download.setText('下載')
                self.widget.pushButton_download.setEnabled(True)

    def trig_download(self):
        url = 'https://gitlab.com/api/v4/projects/{pid}/releases/{tag}/assets/links'.format(
            pid=PID,
            tag=self.least_version
        )
        r = requests.get(url).json()

        url = r[0]['url']
        filename = r[0]['name']

        self.dth.start(url, filename)

    def termNumApply(self):
        num = int(self.widget.comboBox_termNum.currentText())
        self.signalTermNumApply.emit(num)

    def ui_download_start(self, times):
        self.widget.progressBar_download.setValue(0)
        self.widget.progressBar_download.setMaximum(times)
